vcpkg_download_distfile(ARCHIVE
    URLS "https://github.com/apache/incubator-mxnet/releases/download/1.6.0/apache-mxnet-src-1.6.0-incubating.tar.gz"
    FILENAME "apache-mxnet-src-1.6.0-incubating.tar.gz"
    SHA512 851c71b181a5b0d964416ed4165fa9e588228cd84b5a28a8d02c18524e85a2b75dbc95b0152fa461be56a8bbccf261da1759535b9760a365bef08e25cc050018
)

vcpkg_extract_source_archive_ex(
    OUT_SOURCE_PATH SOURCE_PATH
    ARCHIVE ${ARCHIVE}
    PATCHES
        "${CMAKE_CURRENT_LIST_DIR}/0001-static-dmlc-dynamic-runtime.patch"
        "${CMAKE_CURRENT_LIST_DIR}/0002-fix-find-openblas.patch"
        "${CMAKE_CURRENT_LIST_DIR}/0003-manual-cuda-arch-flags.patch"
        "${CMAKE_CURRENT_LIST_DIR}/0004-fix-python-dll-search-path.patch"
)

set(WITH_CUDA OFF)
if("cuda" IN_LIST FEATURES)
  set(WITH_CUDA ON)
endif()

set(WITH_CUDNN OFF)
if("cudnn" IN_LIST FEATURES)
  set(WITH_CUDNN ON)
endif()

set(WITH_OPENCV OFF)
if("opencv" IN_LIST FEATURES)
  set(WITH_OPENCV ON)
endif()

set(WITH_DIST_KVSTORE OFF)
if("dist_kvstore" IN_LIST FEATURES)
  set(WITH_DIST_KVSTORE ON)
endif()

set(WITH_TENSORRT OFF)
if("tensorrt" IN_LIST FEATURES)
  set(WITH_TENSORRT ON)
endif()

set(WITH_CPP OFF)
if("cpp" IN_LIST FEATURES)
  set(WITH_CPP ON)
  # Python 3 required to generate operator wrappers
  vcpkg_find_acquire_program(PYTHON3)
  get_filename_component(PYTHON3_EXE_PATH ${PYTHON3} DIRECTORY)
  set(ENV{PATH} "$ENV{PATH};${PYTHON3_EXE_PATH}")
  # Generated dll will be dynamically loaded from python 3, dependencies must be in PATH
  # package dependencies:
  SET(ENV{PATH} "$ENV{PATH};${CURRENT_INSTALLED_DIR}/bin")
  # cuda runtime dependencies
  if("cuda" IN_LIST FEATURES)
    SET(ENV{PATH} "$ENV{PATH};$ENV{CUDA_PATH}/bin")
  endif()
endif()

vcpkg_configure_cmake(
    PREFER_NINJA
    SOURCE_PATH ${SOURCE_PATH}
    OPTIONS
        -DCMAKE_CXX_STANDARD=14 -DUSE_CXX14_IF_AVAILABLE=ON
        -DUSE_CUDA=${WITH_CUDA} -DENABLE_CUDA_RTC=${WITH_CUDA}
        -DUSE_CUDNN=${WITH_CUDNN}
        -DCUDA_ARCH_FLAGS="-gencode;arch=compute_61,code=sm_61;-gencode;arch=compute_30,code=compute_30;-gencode;arch=compute_52,code=compute_52;-gencode;arch=compute_75,code=compute_75"
        -DUSE_SPLIT_ARCH_DLL=OFF
        -DUSE_NCCL=OFF
        -DUSE_OPENCV=${WITH_OPENCV}
        -DUSE_OPENMP=ON
        -DUSE_LAPACK=OFF
        -DUSE_MKL_IF_AVAILABLE=OFF
        -DUSE_DIST_KVSTORE=${WITH_DIST_KVSTORE}
        -DUSE_CPP_PACKAGE=${WITH_CPP}
        -DUSE_TENSORRT=${WITH_TENSORRT}
        -DUSE_SIGNAL_HANDLER=ON
        -DBUILD_CPP_EXAMPLES=OFF
        -DUSE_GPERFTOOLS=OFF
        -DUSE_JEMALLOC=OFF
        -DENABLE_TESTCOVERAGE=OFF
        -DBUILD_TESTING=OFF
        -DDMLC_FORCE_SHARED_CRT=ON
)


vcpkg_install_cmake()

vcpkg_fixup_cmake_targets()

vcpkg_copy_pdbs()

# Clean
file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/include)
file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/share)
file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/lib/cmake)

file(COPY ${SOURCE_PATH}/LICENSE DESTINATION ${CURRENT_PACKAGES_DIR}/share/mxnet)
file(RENAME ${CURRENT_PACKAGES_DIR}/share/mxnet/LICENSE ${CURRENT_PACKAGES_DIR}/share/mxnet/copyright)
