set(VCPKG_TARGET_ARCHITECTURE x64)
set(VCPKG_CRT_LINKAGE dynamic)

set(VCPKG_BUILD_TYPE release)
set(VCPKG_PLATFORM_TOOLSET "v141")
set(VCPKG_PLATFORM_TOOLSET_VERSION "14.16")

if(PORT STREQUAL "hdf5" OR
   PORT STREQUAL "boost" OR PORT MATCHES "^boost-")
	set(VCPKG_LIBRARY_LINKAGE static)
else()
	set(VCPKG_LIBRARY_LINKAGE dynamic)
endif()
